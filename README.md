# Lys - Sentiment Analysis

Lys is an ambitious project aiming to create an artificial intelligence (AI) model for sentiment analysis in texts. This sophisticated model is designed to accurately determine whether a text expresses positive, negative, or neutral emotions. By using annotated datasets, this project focuses on training a high-performing model capable of decoding natural language nuances and predicting sentiments associated with new texts.

## Project Objective

The primary goal of this project is to develop a robust and reliable AI model capable of understanding and evaluating emotions expressed in various types of text. This initiative aims to provide a practical tool to quickly assess sentiments in product reviews, social reactions, and more. By contributing to a better understanding of emotional nuances in texts, this project aspires to enhance recommendation systems, content filtering, and the understanding of emotional trends across diverse domains.

## Key Features

- **Sentiment Classification:** Precise evaluation of the emotional tone of texts, determining whether they are positive, negative, or neutral.
  
- **Data Preprocessing:** Cleaning and preparing texts for efficient analysis, including tokenization, stopword removal, and normalization.
  
- **Model Training:** Using machine learning algorithms to improve the model's accuracy and generalization.
  
- **Accuracy Evaluation:** Regular assessment of the model's performance to ensure its reliability.
  
- **Trained Model Usage:** Integration of the model into a user-friendly interface for a quick evaluation of expressed sentiment.
  
- **Expansion and Continuous Improvement:** Ability to extend model features to support other languages or text types and refine its accuracy with new data.

## Technical Constraints

- **Programming Language:** Python.
- **Libraries:** Utilization of PyTorch for neural network models and NLTK (Natural Language Toolkit) for natural language processing.

## Project Team

- **Project Lead:** Cédric Payet
- **Developer:** Cédric Payet

## Contact

- **Cédric Payet:** cedricpayet.dev@gmail.com
